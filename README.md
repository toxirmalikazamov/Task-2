# Assignment 2:
## Task list
1. Write a program that is divisible by 3 and 5 <br>

2. Write a program that checks whether given number odd or even.<br>

3. Write a program that get array as argument and sorts it by order // [1,2,3,4...]<br>

4. Write a program that return unique set of array:
input: =>  [
    {test: ['a', 'b', 'c', 'd']},
    {test: ['a', 'b', 'c']},
    {test: ['a', 'd']},
    {test: ['a', 'b', 'k', 'e', 'e']},
    ]

output: => [‘a’, ‘b’, ‘c’, ‘d’, ‘k’, ‘e’]<br>

## License

